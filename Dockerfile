FROM maslick/minimalka:jdk11
WORKDIR /app
EXPOSE 8080
COPY service/ .
CMD ./bin/service